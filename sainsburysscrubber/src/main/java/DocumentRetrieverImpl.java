import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;

public class DocumentRetrieverImpl implements DocumentRetriever {

    public Document get(String URL) throws IOException, IllegalArgumentException {
        return Jsoup.connect(URL).get();
    }

}
