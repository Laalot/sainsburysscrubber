import exceptions.ScrubberException;
import models.PageItem;
import models.PageItemCollection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scrubber {

    public Scrubber(DocumentRetriever retriever){
        this.retriever = retriever;
    }

    private DocumentRetriever retriever;
    private PageItemCollection pageItemCollection = new PageItemCollection();

    public PageItemCollection scrub(String URL) throws ScrubberException {

        Document page;
        ArrayList<Document> productPages;
        PageItemCollection pageItemCollection;

        try {
            page = retriever.get(URL);
        } catch (Exception e) {
            throw new ScrubberException(URL, e.toString());
        }

        productPages = getProductLinksFromPage(page);
        pageItemCollection = constructPageItems(productPages);
        pageItemCollection.calculateTotal();

        return pageItemCollection;
    }

    ArrayList<Document> getProductLinksFromPage(Document page) throws ScrubberException {
        Elements productLinks = page.select(".productNameAndPromotions > h3 > a");

        ArrayList<Document> productPages = new ArrayList<>();

        for (Element productLink: productLinks) {
            String productURL = productLink.absUrl("href");
            try {
                productPages.add(retriever.get(productURL));
            } catch (Exception e) {
                throw new ScrubberException(productURL, e.toString());
            }
        }

        return productPages;
    }

    PageItemCollection constructPageItems(ArrayList<Document> productPages) throws ScrubberException {

        for (Document page : productPages) {

            PageItem item = new PageItem();

            //All product titles are formatted identically - no additions needed.
            try {
                item.setProductName(getProductName(page));
            } catch (Exception e) {
                throw new ScrubberException(page.baseUri(), "Error scrubbing product name: " + e.toString());
            }

            try {
                item.setCaloriesPer100G(getCaloriesPer100G(page));
            } catch (Exception e) {
                throw new ScrubberException(page.baseUri(), "Error scrubbing calories: " + e.toString());
            }

            try {
                item.setPrice(getPrice(page));
            } catch (Exception e) {
                throw new ScrubberException(page.baseUri(), "Error scrubbing price: " + e.toString());
            }

            try {
                item.setDescription(getDescription(page));
            } catch (Exception e) {
                throw new ScrubberException(page.baseUri(), "Error scrubbing product description: " + e.toString());
            }

            pageItemCollection.pageItems.add(item);
        }

        return pageItemCollection;

    }

    String getProductName(Document page) throws ScrubberException {
        Elements productTitleDescription = page.select(".productTitleDescriptionContainer > h1");
        if(!productTitleDescription.isEmpty()) {
            return productTitleDescription.get(0).text();
        }

        throw new ScrubberException(page.baseUri(), "No matching template for product name.");
    }

    Integer getCaloriesPer100G(Document page) throws ScrubberException {
        Elements tdContainingKcal = page.select("td:contains(kcal)");
        if (!tdContainingKcal.isEmpty()) {
            return sanitizeCalories(tdContainingKcal.text());
        }
        Elements classNutritionTable = page.select(".nutritionTable");
        if (!classNutritionTable.isEmpty()) {
            return sanitizeCalories(classNutritionTable.select("tbody > tr > td").get(3).text());
        }

        return null;
    }

    Double getPrice(Document page) throws ScrubberException {

        Elements pricePerUnit = page.select(".pricePerUnit");
        if(!pricePerUnit.isEmpty()) {
            return sanitizePrice(pricePerUnit.first().text());
        }

        throw new ScrubberException(page.baseUri(), "No matching template for price.");
    }

    String getDescription(Document page) throws ScrubberException {
        Elements itemTypeGroupContainer = page.select(".itemTypeGroupContainer.productText");
        if (!itemTypeGroupContainer.isEmpty()) {

            Element firstDiv = itemTypeGroupContainer.select("> div").first();
            if (firstDiv.hasClass("memo")) {
                return sanitizeDescription(firstDiv.select(" > p").first().text());
            } else if (firstDiv.hasClass("itemTypeGroup")) {
                return sanitizeDescription(firstDiv.select("> p").get(1).text());
            }

        }

        Elements productText = page.select(".productText > p");
        if(!productText.isEmpty()) {
            return sanitizeDescription(productText.first().text());
        }

        throw new ScrubberException(page.baseUri(), "No matching template for product description.");
    }

    Integer sanitizeCalories(String calories) {
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher;

        matcher = pattern.matcher(calories);
        while (matcher.find()) {
            return Integer.valueOf(matcher.group(0));
        }

        return null;
    }

    Double sanitizePrice(String price) {
        Pattern pattern = Pattern.compile("(\\d+(?:\\.\\d+)?)");
        Matcher matcher;

        matcher = pattern.matcher(price);
        while (matcher.find()) {
            return Double.valueOf(matcher.group(0));
        }

        return 0.0;
    }

    String sanitizeDescription(String description) {
        return description.split("\\R")[0];
    }

}
