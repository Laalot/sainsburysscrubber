package exceptions;

public class ScrubberException extends Exception {

    public String getURL() {
        return URL;
    }

    String URL;

    @Override
    public String getMessage() {
        return message;
    }

    String message;

    public ScrubberException(String URL, String message) {
        this.URL = URL;
        this.message = message;
    }

}
