import com.google.gson.Gson;
import exceptions.ScrubberException;

public class SainsburysScrubber {

    public static void main(String args[]) {
        if (args.length < 1) {
            System.out.println("No URL: Please provide page URL as first argument.");
            return;
        }

        String pageURL = args[0];
        Scrubber scrubber = new Scrubber(new DocumentRetrieverImpl());
        try {
            Gson gson = new Gson();
            String output = gson.toJson(scrubber.scrub(pageURL));
            System.out.print(output);
        } catch (ScrubberException se) {
            System.out.println("Error while processing URL <" + se.getURL() + "> " + se.getMessage());
        }

    }

}
