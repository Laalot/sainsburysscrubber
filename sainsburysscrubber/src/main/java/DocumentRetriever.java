import org.jsoup.nodes.Document;
import java.io.IOException;

public interface DocumentRetriever {

    Document get(String URL) throws IOException, IllegalArgumentException;

}
