package models;

import com.google.gson.annotations.SerializedName;

public class PageItem {

    @SerializedName("title")
    String productName;
    @SerializedName("kcal_per_100g")
    Integer caloriesPer100G;
    @SerializedName("unit_price")
    double price;
    @SerializedName("description")
    String description;


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getCaloriesPer100G() {
        return caloriesPer100G;
    }

    public void setCaloriesPer100G(Integer caloriesPer100G) {
        this.caloriesPer100G = caloriesPer100G;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
