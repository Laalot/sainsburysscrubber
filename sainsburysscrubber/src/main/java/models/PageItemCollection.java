package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PageItemCollection {
    @SerializedName("results")
    public ArrayList<PageItem> pageItems = new ArrayList<>();

    @SerializedName("total")
    public double total;

    public void calculateTotal() {
        total = pageItems.stream().mapToDouble(o -> o.getPrice()).sum();
    }

    public int size() {
        return pageItems.size();
    }
}
