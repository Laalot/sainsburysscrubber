import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import exceptions.ScrubberException;
import models.PageItem;
import models.PageItemCollection;
import org.junit.Test;
import org.mockito.Mockito;

import javax.swing.text.Document;
import java.io.IOException;

public class ScrubberTests {

    public ScrubberTests() {
        TestConsts.baseDoc.setBaseUri(TestConsts.URL);
        TestConsts.multipleDoc.setBaseUri(TestConsts.URL);
    }

    @Test
    public void commonTemplateResultScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.commonTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals("Sainsbury's Strawberries 400g", pi.getProductName());
        assertEquals(Integer.valueOf(33), Integer.valueOf(pi.getCaloriesPer100G()));
        assertEquals(1.75, pi.getPrice(), 0);
        assertEquals("by Sainsbury's strawberries", pi.getDescription());

    }

    @Test
    public void memoDescriptionResultScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.memoDescriptionTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals("Cherries", pi.getDescription());
    }

    @Test
    public void statementDescriptionResultScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.statementDescriptionTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals("Union Flag", pi.getDescription());
    }

    @Test
    public void tdContainingKcalResultScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.tdContainingKcalTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals(Integer.valueOf(32), Integer.valueOf(pi.getCaloriesPer100G()));
    }

    @Test
    public void nutritionTableResultScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.nutritionTableTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals(Integer.valueOf(52), Integer.valueOf(pi.getCaloriesPer100G()));
    }

    @Test
    public void productNameScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.commonTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals("Sainsbury's Strawberries 400g", pi.getProductName());
    }

    @Test
    public void productPriceScrubsSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.commonTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        PageItem pi = pic.pageItems.get(0);

        assertEquals(1.75, pi.getPrice(), 0);
    }

    @Test
    public void totalIsCalculatedSuccessfully() throws ScrubberException, IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.multipleDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.commonTemplateDoc);

        PageItemCollection pic = scrubber.scrub(TestConsts.URL);
        pic.calculateTotal();

        assertEquals(5.25, pic.total, 0);
    }

    @Test
    public void missingProductNameReturnsError() throws IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.missingProductNameDoc);

        try {
            scrubber.scrub(TestConsts.URL);
        } catch (Exception e) {
            assertEquals("Error scrubbing product name: exceptions.ScrubberException: No matching template for product name.", e.getMessage());
        }

    }

    @Test
    public void missingProductDescriptionReturnsError() throws IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.missingProductDescriptionDoc);

        try {
            scrubber.scrub(TestConsts.URL);
        } catch (Exception e) {
            assertEquals("Error scrubbing product description: exceptions.ScrubberException: No matching template for product description.", e.getMessage());
        }

    }

    @Test
    public void missingPriceReturnsError() throws IOException {
        DocumentRetriever mockRetriever = Mockito.mock(DocumentRetriever.class);

        Scrubber scrubber = new Scrubber(mockRetriever);

        when(mockRetriever.get(TestConsts.URL)).thenReturn(TestConsts.baseDoc);
        when(mockRetriever.get(TestConsts.ProductURL)).thenReturn(TestConsts.missingPriceDoc);

        try {
            scrubber.scrub(TestConsts.URL);
        } catch (Exception e) {
            assertEquals("Error scrubbing price: exceptions.ScrubberException: No matching template for price.", e.getMessage());
        }

    }

}
