# Sainsbury's Scrubber
A utility to generate a JSON array from the products listed on a Sainsbury's store page.
## Getting Started
First, clone the repository. Then build the package using Maven.
### Prerequisites

The pre-requisites for the solution as as follows;

```
Java 1.8+
Maven
```

All other dependencies are managed by Maven, but are listed later in this readme.

### Installing

First, ensure Java (1.8+) is [installed](https://java.com/en/download/ "Java"), and install maven following the instructions [here](https://maven.apache.org/ "Apache Maven").
Once this is done, navigate to the project directory (where the pom.xml is located) and run ```mvn package```.
This will build the project, and produce a .jar in the 'target' folder.
You may now navigate to this folder and run the solution, specifying the URL you wish to scrub as the first argument, e.g;
```
java -jar sainsburysscrubber-1.0-SNAPSHOT.jar https://url.domain.com
```

Example output file is available in the root directory.
## Running the tests

The tests are run automatically when ```mvn package ``` is run.

## Dependencies
* [Maven](https://maven.apache.org/, "Github") - Dependency Management
* [gson-2.83](https://github.com/google/gson, "Github") - Serialization
* [jsoup-1.11.3](https://jsoup.org/, "Github") - HTML Parsing
* [junit-4.12](https://github.com/junit-team/junit4, "Github") - Unit Testing
  * [hamcrest-core-1.3](https://github.com/hamcrest/JavaHamcrest, "Github") - Dependency of JUnit
* [mockito-all-1.9.5](https://github.com/mockito/mockito, "Github") - Mocking

